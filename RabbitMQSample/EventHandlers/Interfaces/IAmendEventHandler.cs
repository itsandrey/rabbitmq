﻿using RabbitMQSample.Events.Interfaces;
using System.Threading.Tasks;

namespace RabbitMQSample.EventHandlers.Interfaces
{
    public interface IAmendEventHandler<in T>
        where T : IAmendEvent
    {
        Task HandleAsync(T model);
    }
}
