﻿using Microsoft.Extensions.Logging;
using RabbitMQSample.EventHandlers.Interfaces;
using RabbitMQSample.Events;
using System.Threading.Tasks;

namespace RabbitMQSample.EventHandlers
{
    public class AmendUserEventHandler : IAmendEventHandler<AmendUserEvent>
    {
        private readonly ILogger<AmendUserEventHandler> _logger;

        public AmendUserEventHandler(ILogger<AmendUserEventHandler> logger)
        {
            _logger = logger;
        }

        public Task HandleAsync(AmendUserEvent model)
        {
            _logger.LogInformation($"Handle amend User with value: {model.Id} {model.Name}");
            return Task.CompletedTask;
        }
    }
}
