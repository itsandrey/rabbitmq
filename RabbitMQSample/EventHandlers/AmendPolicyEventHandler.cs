﻿using Microsoft.Extensions.Logging;
using RabbitMQSample.EventHandlers.Interfaces;
using RabbitMQSample.Events;
using System.Threading.Tasks;

namespace RabbitMQSample.EventHandlers
{
    public class AmendPolicyEventHandler : IAmendEventHandler<AmendPolicyEvent>
    {
        private readonly ILogger<AmendPolicyEventHandler> _logger;

        public AmendPolicyEventHandler(ILogger<AmendPolicyEventHandler> logger)
        {
            _logger = logger;
        }

        public Task HandleAsync(AmendPolicyEvent model)
        {
            _logger.LogInformation($"Handle amend Policy with value: {model.Id} {model.Number}");
            return Task.CompletedTask;
        }
    }
}
