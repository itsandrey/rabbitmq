﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using RabbitMQSample.EventBus.Interfaces;
using RabbitMQSample.Events;
using RabbitMQSample.Models;

namespace RabbitMQSample.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MainController : ControllerBase
    {
        private readonly IEventBus _eventBus;
        private readonly ILogger<MainController> _logger;

        public MainController(ILogger<MainController> logger, IEventBus eventBus)
        {
            _logger = logger;
            _eventBus = eventBus;
        }

        [HttpPost("Amend-User")]
        public void AmendUser([FromBody] AmendUserEvent model)
        {
            _eventBus.Publish(model);
        }

        [HttpPost("Amend-Policy")]
        public void AmendPolicy([FromBody] AmendPolicyEvent model)
        {
            _eventBus.Publish(model);
        }
    }
}
