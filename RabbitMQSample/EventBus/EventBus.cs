﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQSample.EventBus.Interfaces;
using RabbitMQSample.EventHandlers.Interfaces;
using RabbitMQSample.Events.Interfaces;
using RabbitMQSample.Models.Settings;
using System;
using System.Text;

namespace RabbitMQSample.EventBus
{
    public class EventBus : IEventBus
    {
        private readonly IConnection _connection;
        private IModel _channel;
        private readonly IServiceProvider _serviceProvider;
        private readonly IConfiguration _configuration;

        public IModel Channel
        {
            get
            {
                if (_channel == null)
                    _channel = _connection.CreateModel();

                return _channel;
            }
        }

        public EventBus(IServiceProvider provider, IConfiguration configuration)
        {
            _configuration = configuration;

            var settings = new EventBusSettings();
            _configuration.GetSection("EventBusSettings").Bind(settings);

            var connectionFactory = new ConnectionFactory
            {
                HostName = settings.HostName,
                Port = settings.Port,
                UserName = settings.UserName,
                Password = settings.Password,
                DispatchConsumersAsync = true
            };

            _connection = connectionFactory.CreateConnection();
            _serviceProvider = provider;
        }

        public void Publish(IAmendEvent e)
        {
            var settings = new SenderQueueSettingsModel();
            _configuration.GetSection("SenderQueueSettings").Bind(settings);

            CreateExchangeIfNotExists(settings);

            var jsonEvent = JsonConvert.SerializeObject(e);
            var bytesEvent = Encoding.UTF8.GetBytes(jsonEvent);

            Channel.BasicPublish(settings.ExchangeName, string.Empty, body: bytesEvent);
        }

        public void Subscribe<TH, TE>()
            where TH : IAmendEventHandler<TE>
            where TE : IAmendEvent
        {
            var settings = new SenderQueueSettingsModel();
            _configuration.GetSection("SenderQueueSettings").Bind(settings);

            BindQueue(settings);

            var consumer = new AsyncEventingBasicConsumer(Channel);
            consumer.Received += async (obj, args) =>
            {
                using (var scope = _serviceProvider.CreateScope())
                {
                    var handler = scope.ServiceProvider.GetRequiredService<IAmendEventHandler<TE>>();
                    var jsonMessage = Encoding.UTF8.GetString(args.Body.ToArray());
                    var result = JsonConvert.DeserializeObject<TE>(jsonMessage);

                    await handler.HandleAsync(result);
                    Channel.BasicAck(args.DeliveryTag, false);
                }
            };

            Channel.BasicConsume(settings.Name, settings.AutoAck, consumer);
        }

        private void BindQueue(SenderQueueSettingsModel model)
        {
            CreateExchangeIfNotExists(model);

            Channel.QueueDeclare(model.Name, model.Durable, model.Exclusive, model.AutoDelete);
            Channel.QueueBind(model.Name, model.ExchangeName, model.RoutingKey);
        }

        private void CreateExchangeIfNotExists(SenderQueueSettingsModel model)
        {
            Channel.ExchangeDeclare(model.ExchangeName, ExchangeType.Fanout, model.Durable);
        }
    }
}
