﻿using RabbitMQSample.EventHandlers.Interfaces;
using RabbitMQSample.Events.Interfaces;

namespace RabbitMQSample.EventBus.Interfaces
{
    public interface IEventBus
    {
        void Publish(IAmendEvent e);

        void Subscribe<TH, TE>()
            where TH : IAmendEventHandler<TE>
            where TE : IAmendEvent;
    }
}
