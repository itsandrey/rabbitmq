﻿using System.Collections.Generic;

namespace RabbitMQSample.Models.Settings
{
    public class SenderQueueSettingsModel
    {
        public string Name { get; set; }
        public string ExchangeName { get; set; }
        public bool Durable { get; set; }
        public bool Exclusive { get; set; }
        public bool AutoDelete { get; set; }
        public bool AutoAck { get; set; }
        public Dictionary<string, object> Arguments { get; set; }
        public string RoutingKey { get; set; }
        public bool Enabled { get; set; }
    }
}
