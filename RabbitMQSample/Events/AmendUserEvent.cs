﻿using RabbitMQSample.Events.Interfaces;

namespace RabbitMQSample.Events
{
    public class AmendUserEvent : IAmendEvent
    {
        public int Id { get; set; }

        public string Name { get; set; }
    }
}
