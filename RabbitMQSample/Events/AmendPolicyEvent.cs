﻿using RabbitMQSample.Events.Interfaces;

namespace RabbitMQSample.Events
{
    public class AmendPolicyEvent : IAmendEvent
    {
        public int Id { get; set; }

        public string Number { get; set; }
    }
}
